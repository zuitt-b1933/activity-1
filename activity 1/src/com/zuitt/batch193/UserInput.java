package com.zuitt.batch193;

import java.util.Scanner;

public class UserInput {
    public static void main(String[] args) {
        Scanner appScanner = new Scanner(System.in);
        System.out.println("First Name:");
        String firstName = appScanner.nextLine().trim();

        System.out.println("Last Name:");
        String lastName = appScanner.nextLine().trim();

        System.out.println("First Subject Grade:");
        double firstSubjectGrade = appScanner.nextDouble();

        System.out.println("Second Subject Grade:");
        double secondSubjectGrade = appScanner.nextDouble();

        System.out.println("Third Subject Grade:");
        double thirdSubjectGrade = appScanner.nextDouble();

        System.out.println("Good day, " + firstName + lastName);
        double average = (double) ((firstSubjectGrade + secondSubjectGrade + thirdSubjectGrade)/3);
        System.out.println("Your Grade Average is: " + average);


    }
}
